# Scratch purge

With *scratch-purge*, a scratch folder can be purged automatically. A scratch folder is a folder where users can put files which are automatically deleted after a configurable time such as:

* Shared folder between team members of temporary files
* Shared scratch folder on computer cluster

*scratch-purge* is executed periodically to purge old files. It keeps track of files using a [LevelDB](https://github.com/syndtr/goleveldb) database and is written in [Go](https://golang.org).

## Download

See [tags](/../tags) page.

## Compilation

Compile the program with:

``` bash
go get -d github.com/syndtr/goleveldb/leveldb
go build src/scratch_purge.go
```

## Archlinux

A PKGBUILD is available in Archlinux [AUR](https://aur.archlinux.org/packages/scratch-purge).

## Usage

``` bash
scratch_purge -target_path /scratch/content \  # The path to the scratch folder
              -db_path /scratch/content.db \   # DB path
              -delay 259000 \                  # How long or how old files can be kept in scratch (seconds)
              -trash_path /scratch/trash \     # Keep files in this trash folder before removing them
              -trash_increment 10 \            # How many trash folders should be kept before deletion
              -ignore .stfolder,.stignore \    # These files will be ignored by scratch-purge
              -report                          # Print report
```

## Scheduling purge jobs

A *systemd* timer can be used to periodically (e.g. every day) start *scratch-purge*. Example of required service and timer files can be found in *systemd* folder for */scratch* example. These two files needs to be copied in systemd config folder (typically `/etc/systemd/system`). The timer then needs to enabled/started:
``` bash
systemctl enable scratch_purge.timer
```

## License

*scratch-purge* is distributed under the Mozilla Public License Version 2.0 (see /LICENSE).

Copyright (C) 2018 Charles E. Vejnar
