//
// Copyright (C) 2018 Charles E. Vejnar
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

package main

import (
	"encoding/binary"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
)

const Debug = false

type PathInfo struct {
	Name  string
	Mod   int64
	IsDir bool
}

func IsEmpty(path string) (bool, error) {
	f, err := os.Open(path)
	if err != nil {
		return false, err
	}
	defer f.Close()

	_, err = f.Readdirnames(1)
	if err == io.EOF {
		return true, nil
	}
	return false, err
}

func main() {
	// Arguments
	var targetPath, dbPath, trashPathRoot, ignoreFilesRaw string
	var delay, trashIncrement int
	var report bool
	flag.StringVar(&dbPath, "db_path", "", "Database path")
	flag.StringVar(&targetPath, "target_path", "", "Target path")
	flag.StringVar(&trashPathRoot, "trash_path", "", "Trash path")
	flag.StringVar(&ignoreFilesRaw, "ignore", "", "File(s) to ignore (comma separated)")
	flag.IntVar(&delay, "delay", 60, "Delay")
	flag.IntVar(&trashIncrement, "trash_increment", 10, "Trash increment")
	flag.BoolVar(&report, "report", false, "Print report")
	// Arguments: Parse
	flag.Parse()

	// Check arguments
	if len(targetPath) == 0 {
		fmt.Println("Target path argument is mandatory")
		os.Exit(1)
	} else if _, err := os.Stat(targetPath); os.IsNotExist(err) {
		fmt.Println("Target path not found:", targetPath)
		os.Exit(1)
	}
	if len(dbPath) == 0 {
		dbPath = targetPath + ".db"
	}

	// Parse ignored file(s)
	ignoreFiles := make(map[string]bool)
	if len(ignoreFilesRaw) > 0 {
		for _, f := range strings.Split(ignoreFilesRaw, ",") {
			ignoreFiles[f] = true
		}
	}

	// Open DB
	db, err := leveldb.OpenFile(dbPath, nil)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// Get current time
	currentTime := time.Now().Unix()
	buf := make([]byte, binary.MaxVarintLen64)
	n := binary.PutVarint(buf, currentTime)
	currentTimeBuffer := buf[:n]
	// Compute purge time
	purgeTime := currentTime - int64(delay)
	if Debug {
		fmt.Println("Current time", currentTime)
		fmt.Println("Purge time", purgeTime)
	}

	// Trash folder
	var trashPath string
	if trashPathRoot != "" {
		trashPath = filepath.Join(trashPathRoot, fmt.Sprintf("%d", currentTime))
		// Create trash folder
		if err := os.MkdirAll(trashPath, 0755); err != nil {
			panic(err)
		}
		// Remove older trash folder(s)
		if files, err := ioutil.ReadDir(trashPathRoot); err != nil {
			panic(err)
		} else {
			for i := 0; i < len(files)-trashIncrement; i++ {
				if Debug {
					fmt.Println("Remove", files[i].Name())
				}
				if err := os.RemoveAll(filepath.Join(trashPathRoot, files[i].Name())); err != nil {
					panic(err)
				}
			}
		}
	}

	// Walk through files and get their modification time
	var pathsMod []PathInfo
	err = filepath.Walk(targetPath, func(path string, f os.FileInfo, err error) error {
		if _, ok := ignoreFiles[filepath.Base(path)]; !ok && targetPath != path {
			pathsMod = append(pathsMod, PathInfo{path, f.ModTime().Unix(), f.IsDir()})
		}
		return err
	})
	if err != nil {
		panic(err)
	}

	// Report
	var nScratch, nNew, nRemove int

	// Purge files and folders
	for i := len(pathsMod) - 1; i >= 0; i-- {
		pm := pathsMod[i]
		path := pm.Name
		// Count all files
		if !pm.IsDir {
			nScratch++
		}
		// Find path in DB
		data, err := db.Get([]byte(path), nil)
		if err == leveldb.ErrNotFound {
			if Debug {
				fmt.Println("Add DB", path)
			}
			if err = db.Put([]byte(path), currentTimeBuffer, nil); err != nil {
				panic(err)
			}
			if !pm.IsDir {
				nNew++
			}
		} else if err != nil {
			panic(err)
		} else {
			dbTime, _ := binary.Varint(data)
			// Path (only file) has been updated
			if dbTime < pm.Mod && !pm.IsDir {
				if Debug {
					fmt.Println("Update DB", path)
				}
				if err = db.Put([]byte(path), currentTimeBuffer, nil); err != nil {
					panic(err)
				}
				dbTime = currentTime
			}
			// Path is too old
			if dbTime < purgeTime {
				// Remove from FS
				if pm.IsDir {
					if empty, err := IsEmpty(path); err != nil {
						panic(err)
					} else if empty {
						if Debug {
							fmt.Println("Remove folder", path)
						}
						if err = os.Remove(path); err != nil {
							panic(err)
						}
						// Remove from DB
						if Debug {
							fmt.Println("Remove from DB", path)
						}
						if err = db.Delete([]byte(path), nil); err != nil {
							panic(err)
						}
					}
				} else {
					if Debug {
						fmt.Println("Remove", path)
					}
					if trashPath == "" {
						if err = os.Remove(path); err != nil {
							panic(err)
						}
					} else {
						newPath := filepath.Join(trashPath, filepath.Base(path))
						if err = os.Rename(path, newPath); err != nil {
							panic(err)
						}
					}
					// Remove from DB
					if Debug {
						fmt.Println("Remove from DB", path)
					}
					if err = db.Delete([]byte(path), nil); err != nil {
						panic(err)
					}
					nRemove++
				}
			}
		}
	}

	// Clean DB from removed path
	iter := db.NewIterator(nil, nil)
	for iter.Next() {
		key := iter.Key()
		if _, err := os.Stat(string(key)); os.IsNotExist(err) {
			if Debug {
				fmt.Println("Remove from DB", string(key))
			}
			if err = db.Delete([]byte(key), nil); err != nil {
				panic(err)
			}
		}
	}

	// Compact DB
	db.CompactRange(util.Range{nil, nil})

	// Print report
	if report {
		fmt.Printf("Scratch total:%d new:%d removed:%d\n", nScratch, nNew, nRemove)
	}
}
